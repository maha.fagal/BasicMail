/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import DB_util.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author N
 */
public class Inbox {

    int userID;
    ArrayList<Mail> mails = new ArrayList<Mail>();

    public Inbox() {

    }

    public int getUserID() {
        return userID;
    }

    public ArrayList<Mail> getMailsIDs() {
        return mails;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public static ArrayList<Mail> getInbox(String address) {
        try {
            String query = "SELECT `user_has_mail`.`email_id` FROM `user_has_mail` WHERE `user_has_mail`.`receiver_address` = ? ";
            String query2 = "SELECT * FROM `email` WHERE `email`.`id` = ? ";
            PreparedStatement preparedStatement = null;
            PreparedStatement preparedStatement2 = null;
            System.out.println("called");
            Connection dbConnection = null;
            ResultSet rs = null;
            ResultSet rs2 = null;

            Inbox inbox = new Inbox();
            dbConnection = DBConnection.getConnection();
            preparedStatement = dbConnection.prepareStatement(query);
            //preparedStatement2 = dbConnection.prepareStatement(query2);

            preparedStatement.setString(1, address);

            rs = preparedStatement.executeQuery();

            ArrayList<Integer> mailsID = new ArrayList<Integer>();
            while (rs.next()) {
                mailsID.add(rs.getInt(1));
            }
//            preparedStatement.close();
//            rs.close();

            preparedStatement2 = dbConnection.prepareStatement(query2);
            for (int i = 0; i < mailsID.size(); i++) {
                preparedStatement2.setInt(1, mailsID.get(i));
                rs2 = preparedStatement2.executeQuery();
                while (rs2.next()) {
                    int id = rs2.getInt("id");
                    String subject = rs2.getString("subject");
                    Mail mail = new Mail();
                    mail.setId(id);
                    mail.setSubject(subject);
                    inbox.mails.add(mail);
                }

            }
            System.out.println("Models.Inbox.getInbox()" +address+" "+ inbox.mails.toString());
            return inbox.mails;

        } catch (SQLException ex) {
            Logger.getLogger(Inbox.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }
    
        public static ArrayList<Mail> getArchive(String address) {
        Inbox archived = new Inbox();

        try {
            String query = "SELECT `user_has_mail`.`email_id` FROM `user_has_mail` WHERE `user_has_mail`.`receiver_address` = ? AND `user_has_mail`.`status` = 'archived'";
            String query2 = "SELECT * FROM `email` WHERE `email`.`id` ='?' ";

            PreparedStatement preparedStatement = null;

            Connection dbConnection = null;
            ResultSet rs = null;
            dbConnection = DBConnection.getConnection();
            preparedStatement = dbConnection.prepareStatement(query);

            preparedStatement.setString(1, address);

            rs = preparedStatement.executeQuery();

            ArrayList<Integer> mailsID = new ArrayList<>();
            while (rs.next()) {
                mailsID.add(rs.getInt(1));
            }
            preparedStatement.close();
            rs.close();

            preparedStatement = dbConnection.prepareStatement(query2);

            for (int i = 0; i < mailsID.size(); i++) {
                preparedStatement.setInt(1, mailsID.get(i));
                rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    int id = rs.getInt("id");
                    String subject = rs.getString("subject");
                    Mail mail = new Mail();
                    mail.setId(id);
                    mail.setSubject(subject);
                    archived.mails.add(mail);
                }

            }

            return archived.mails;

        } catch (SQLException ex) {
            Logger.getLogger(Inbox.class.getName()).log(Level.SEVERE, null, ex);
        }
        return archived.mails;

    }
}
