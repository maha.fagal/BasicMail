/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import DB_util.DBConnection;
import DB_util.DBConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maha
 */
public class Mail {

    int id;
    String subject;
    String body;
    java.sql.Date date;
    String sender;
    String reciever;
    String status;

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReciever() {
        return reciever;
    }

    public void setReciever(String reciever) {
        this.reciever = reciever;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Mail(int id, String subject, String body, Date date, String sender, String reciever, String status) {
        this.id = id;
        this.subject = subject;
        this.body = body;
        this.date = date;
        this.sender = sender;
        this.reciever = reciever;
        this.status = status;
    }

    public Mail() {

    }

    public Mail(int id, String subject, String body, Date date) {
        this.id = id;
        this.subject = subject;
        this.body = body;
        this.date = date;
    }

    public Mail(String subject, String body) {
        this.subject = subject;
        this.body = body;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Mail{" + "id=" + id + ", subject=" + subject + ", body=" + body + ", date=" + date + ", sender=" + sender + ", reciever=" + reciever + ", status=" + status + '}';
    }

    public static ArrayList<Mail> showMail(int id) {

        String query = "SELECT email.* FROM email WHERE email.id = ?";
        PreparedStatement preparedStatement = null;
        String query2 = "SELECT `parent_id` FROM `ia_mail`.`threads` where child_id = ?;";
        PreparedStatement preparedStatement2 = null;
        String query3 = "UPDATE `ia_mail`.`user_has_mail` SET `status` = " + " 'read' " + " WHERE email_id = ?;";
        PreparedStatement preparedStatement3 = null;

        boolean flag = false;

        Connection dbConnection = null;
        ResultSet resultSet = null;
        ResultSet resultSet2 = null;
        Mail mail;
        ArrayList<Mail> mails = new ArrayList<Mail>();

        dbConnection = DBConnection.getConnection();
        try {
            preparedStatement = dbConnection.prepareStatement(query);
            preparedStatement2 = dbConnection.prepareStatement(query2);
            preparedStatement3 = dbConnection.prepareCall(query3);

            preparedStatement.setInt(1, id);

            preparedStatement3.setInt(1, id);
            preparedStatement3.executeUpdate();

            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                do {
                    id = resultSet.getInt("id");
                    String subject = resultSet.getString("subject");
                    String body = resultSet.getString("body");
                    Date date = resultSet.getDate("date");

                    mail = new Mail(id, subject, body, date);

                    mails.add(mail);

                    preparedStatement2.setInt(1, id);
                    resultSet2 = preparedStatement2.executeQuery();

                    if (resultSet2.next()) {
                        flag = true;
                        int parent_id = resultSet2.getInt(1);
                        if (parent_id == -1) {
                            flag = false;
                            break;
                        }
                        preparedStatement.setInt(1, parent_id);
                        resultSet = preparedStatement.executeQuery();
                    } else {
                        flag = false;
                        break;
                    }

                } while (resultSet.next() && flag);

            }
            return mails;

        } catch (SQLException ex) {
            Logger.getLogger(Mail.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public static Mail getMail(int id) {

        String query = "SELECT user_has_mail.receiver_address, user_has_mail.sender_address, user_has_mail.status, email.subject, email.body, email.date "
                + "from email inner join user_has_mail  "
                + "on id = email_id "
                + "where email_id = ? ;";

        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        dbConnection = DBConnection.getConnection();
        try {
            preparedStatement = dbConnection.prepareStatement(query);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String reciever = resultSet.getString("receiver_address");
                String sender = resultSet.getString("sender_address");
                String status = resultSet.getString("status");
                String subject = resultSet.getString("subject");
                String body = resultSet.getString("body");
                Date date = resultSet.getDate("date");

                return new Mail(id, subject, body, date, sender, reciever, status);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Mail.class.getName()).log(Level.SEVERE, null, ex);
            return null;

        }
//
        return null;

    }

    public static ArrayList<Mail> searchInboxBySender(String userAddress, String senderAddress) {

        String query = "SELECT `email_id` FROM `ia_mail`.`user_has_mail` where receiver_address = ? AND sender_address = ?;";
        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        dbConnection = DBConnection.getConnection();

        try {
            preparedStatement = dbConnection.prepareStatement(query);
            preparedStatement.setString(1, userAddress);
            preparedStatement.setString(2, senderAddress);
            resultSet = preparedStatement.executeQuery();

            ArrayList<Mail> mails = new ArrayList<Mail>();

            while (resultSet.next()) {
                mails.add(getMail(resultSet.getInt("email_id")));
            }

            return mails;

        } catch (SQLException ex) {
            Logger.getLogger(Mail.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public static ArrayList<Mail> searchSentMailByReciever(String userAddress, String recieverAddress) {
        return searchInboxBySender(userAddress, recieverAddress);
    }

    public static ArrayList<Mail> searchInboxByDate(int userID, java.sql.Date date) {

        String query = "select email_id "
                + "from user_has_mail inner join email "
                + "on user_has_mail.email_id = email.id "
                + "where user_has_mail.sender_id = ?"
                + "   and email.date = ?;";
        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        dbConnection = DBConnection.getConnection();
        try {
            preparedStatement = dbConnection.prepareStatement(query);
            preparedStatement.setInt(1, userID);
            preparedStatement.setDate(2, date);
            resultSet = preparedStatement.executeQuery();

            ArrayList<Mail> mails = new ArrayList<Mail>();

            while (resultSet.next()) {
                mails.add(getMail(resultSet.getInt(1)));
            }

            return mails;

        } catch (SQLException ex) {
            Logger.getLogger(Mail.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public static int sendMail(Mail mail, String senderAddress, String recieverAddress) {
        String query = "INSERT INTO ia_mail.email ( subject , body , date ) VALUES( ? , ? , ?)";
        String query1 = "INSERT INTO `ia_mail`.`user_has_mail` VALUES(?,?,?,'unread')";

        PreparedStatement preparedStatement = null;

        Connection dbConnection = null;
        long result = -1;
        dbConnection = DBConnection.getConnection();
        try {

            preparedStatement = dbConnection.prepareStatement(query);
            preparedStatement.setString(1, mail.getSubject());
            preparedStatement.setString(2, mail.getBody());
            Calendar cal = Calendar.getInstance();

            java.sql.Date date = new java.sql.Date(cal.getTimeInMillis());
            preparedStatement.setDate(3, date);

//            result = preparedStatement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
            System.out.println("Models.Mail.sendMail() ------------" + preparedStatement.toString() + " ");

            result = preparedStatement.executeUpdate();
            preparedStatement.close();
            PreparedStatement getLastInsertId = dbConnection.prepareStatement("SELECT LAST_INSERT_ID()");
            ResultSet rs = getLastInsertId.executeQuery();
            if (rs.next()) {
                result = rs.getLong("last_insert_id()");
            }
            System.out.println("Models.Mail.sendMail() ------------" + result);

            preparedStatement = dbConnection.prepareStatement(query1);
            preparedStatement.setInt(1, (int) result);
            preparedStatement.setString(2, senderAddress);
            preparedStatement.setString(3, recieverAddress);
            System.out.println("Models.Mail.sendMail() ------------" + preparedStatement.toString() + " ");

            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Mail.class.getName()).log(Level.SEVERE, null, ex);

        }
        return (int) result;

    }

    public static int deleteMail(Mail mail, String useAddress) {
        String query = "UPDATE `ia_mail`.`user_has_mail` SET `ia_mail`.`user_has_mail`.'status'='deleted' WHERE 'receiver_address'=? AND 'email_id'=? ";

        PreparedStatement preparedStatement = null;

        Connection dbConnection = null;
        int resultSet = -1;

        dbConnection = DBConnection.getConnection();
        try {
            preparedStatement = dbConnection.prepareStatement(query);

            preparedStatement.setString(1, useAddress);
            preparedStatement.setInt(2, mail.getId());

            resultSet = preparedStatement.executeUpdate();

            preparedStatement.close();
            dbConnection.close();

        } catch (SQLException ex) {
            Logger.getLogger(Mail.class.getName()).log(Level.SEVERE, null, ex);

        }
        return resultSet;
    }

    public static int archiveMail(int mailID, String useAddress) {
        String query = "UPDATE `ia_mail`.`user_has_mail` SET `ia_mail`.`user_has_mail`.status='archived' WHERE 'receiver_address'=? AND 'email_id'=? ";

        PreparedStatement preparedStatement = null;

        Connection dbConnection = null;
        int resultSet = -1;

        dbConnection = DBConnection.getConnection();
        try {
            preparedStatement = dbConnection.prepareStatement(query);

            preparedStatement.setString(1, useAddress);
            preparedStatement.setInt(2, mailID);

            resultSet = preparedStatement.executeUpdate();

            preparedStatement.close();
            dbConnection.close();

        } catch (SQLException ex) {
            Logger.getLogger(Mail.class.getName()).log(Level.SEVERE, null, ex);

        }
        return resultSet;
    }

    public static int forwardMail(Mail mail, String senderAddress, String recieverAddress) {
        String query = "INSERT INTO `ia_mail`.`email`( `subject`, `body`, `date`) VALUES(?,?,?)";
        String query1 = "INSERT INTO `ia_mail`.`user_has_mail` VALUES(?,?,?,'unread')";

        PreparedStatement preparedStatement = null;

        Connection dbConnection = null;
        int result = -1;
        dbConnection = DBConnection.getConnection();
        try {
            preparedStatement = dbConnection.prepareStatement(query);
            preparedStatement.setString(1, mail.getSubject());
            preparedStatement.setString(2, mail.getBody());
            java.sql.Date date = new java.sql.Date(new java.util.Date().getDate());
            preparedStatement.setDate(3, date);
            result = preparedStatement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);

            preparedStatement.close();
            preparedStatement = dbConnection.prepareStatement(query1);
            preparedStatement.setInt(1, result);
            preparedStatement.setString(2, senderAddress);
            preparedStatement.setString(3, recieverAddress);
            result = preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Mail.class.getName()).log(Level.SEVERE, null, ex);

        }
        return result;

    }

    public static int replyMail(Mail mail, String senderAddress, String recieverAddress) {
        int insertedMailID = sendMail(mail, senderAddress, recieverAddress);
        String query = "INSERT INTO `ia_mail`.`threads` VALUES(?,?)";
        PreparedStatement preparedStatement = null;

        Connection dbConnection = null;
        int result = -1;
        dbConnection = DBConnection.getConnection();
        try {
            preparedStatement = dbConnection.prepareStatement(query);
            preparedStatement.setInt(mail.getId(), insertedMailID);
            result = preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Mail.class.getName()).log(Level.SEVERE, null, ex);

        }
        return result;
    }

}
