package Models;

import DB_util.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Yousef
 */
public class User {

    public static String signUp(String fName, String lName, String mail, String pass) {
        try {
            String signup_sql = "INSERT INTO `user` ( `password`, `address`, `name`) VALUES (?,?,?)";
            Connection dbConnection = null;
            PreparedStatement preparedStatement = null;

            dbConnection = DBConnection.getConnection();
            preparedStatement = dbConnection.prepareStatement(signup_sql);
            preparedStatement.setString(1, pass);
            preparedStatement.setString(2, mail);
            preparedStatement.setString(3, fName + " " + lName);

            // execute select SQL stetement
            int rowsInserted = preparedStatement.executeUpdate();

            System.out.println("Record is inserted into User table! " + rowsInserted);
            preparedStatement.close();
            dbConnection.close();

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";

    }

    public static String login(String mail, String pass) {
        Connection dbConnection;
        PreparedStatement preparedStatement = null ;
        ResultSet rs = null ;
        try {
            String login_sql = "SELECT `user`.`address` FROM `user` WHERE `user`.`address` = ? AND `user`.`password`= ? ";

            dbConnection = DBConnection.getConnection();
            preparedStatement = dbConnection.prepareStatement(login_sql);
            preparedStatement.setString(1, mail);
            preparedStatement.setString(2, pass);
             rs = preparedStatement.executeQuery();

            if (rs.next()) {
                return rs.getString("address");
            }

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                preparedStatement.close();
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return "";

    }

    public static boolean editUserName(String mail, String pass, String name) {

        String updateQuery = "UPDATE `ia_mail`.`user` SET`name` = ? WHERE  address = ? and password = ?";
        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;

        dbConnection = DBConnection.getConnection();
        try {
            preparedStatement = dbConnection.prepareStatement(updateQuery);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, mail);
            preparedStatement.setString(3, pass);

            preparedStatement.executeUpdate();
            return true;

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    public static boolean editUserPassword(String mail, String pass, String newPassword) {

        String updateQuery = "UPDATE `ia_mail`.`user` SET`password` = ? WHERE  address = ? and password = ?";
        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;

        dbConnection = DBConnection.getConnection();
        try {
            preparedStatement = dbConnection.prepareStatement(updateQuery);
            preparedStatement.setString(1, newPassword);
            preparedStatement.setString(2, mail);
            preparedStatement.setString(3, pass);

            preparedStatement.executeUpdate();
            return true;

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }
}
