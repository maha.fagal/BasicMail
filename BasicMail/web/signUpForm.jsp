<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Mailing Service</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script language="javascript" type="text/javascript" src="SignUpAjax.js"></script>
        <style type="text/css">
        </style>

    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top" id = "bar">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Mailing Service</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    
                </div><!--/.navbar-collapse -->
            </div>
        </nav>

        <div>
            <div class="container-fluid">
                <section class="container">
                    <div class="container-page">				
                        <div class="col-md-6" id = "a">
                         <!--   <form action = "SignUp" method="post"> -->
                                <h3 class="dark-grey">Registration</h3>

<!--
                                
                                    if (request.getAttribute("error") != null) {
                                        String s = (String) request.getAttribute("error");
                                        if (s.length() > 0) {
                                            out.print("<div class =\"alert alert-warning\">" + s + "</div>");
                                        }
                                    }
                                
                              -->
                                <div id="error_area" class ="alert alert-warning" style="display: none;"></div>
                                
                                <div class="form-group col-lg-6">
                                    <label>First Name</label>
                                    <input type="text" name="firstName" id="firstName" class="form-control" value="">
                                </div>
                                <div class="form-group col-lg-6">
                                    <label>Last Name</label>
                                    <input type="text" name="lastName" id="lastName" class="form-control" value="">
                                </div>

                                <div class="form-group col-lg-12">
                                    <label>Email Address</label>
                                    <input type="email" name="mail" id="mail" class="form-control" value="">
                                </div>		

                                <div class="form-group col-lg-8">
                                    <label>Password</label>
                                    <input type="password" name="pass1" id="pass1" class="form-control" value="">
                                </div>

                                <div class="form-group col-lg-8">
                                    <label>Repeat Password</label>
                                    <input type="password" name="pass2" id="pass2" class="form-control" value="">
                                </div>			
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-primary" onclick="sendSignUpData()" value="Register" />
                                </div>
                        <!--    </form> -->
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </body>

    <script type="text/javascript">

        var mar = 0.07836990595 * $(window).height();
        $("#a").css('margin-top', mar + "px");

    </script>