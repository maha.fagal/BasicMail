<%@page import="java.util.HashMap"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Mailing Service</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        <style type="text/css">
            #full{
                background-image: url("pexels-photo-66367.jpeg"); 
                background-size:cover; 
                background-position:center; 
            }
            #center{
                color: white;
            }
        </style>

    </head>

    <body >

        <%
            HashMap<String, HttpSession> sessionsManager = (HashMap)application.getAttribute("SessionsManager");
            Cookie [] cookies = request.getCookies();
            // Acessing cookie[1] not cookie[0] to skip the first default one created by Tomcat.
            if(cookies != null && cookies.length > 1 && sessionsManager != null && sessionsManager.containsKey(cookies[1].getValue())){
                response.sendRedirect("inbox.jsp");
            }
            else if(cookies != null && cookies.length > 1){
                  Cookie cookie = cookies[1];
                  cookie.setMaxAge(0);
                  response.addCookie(cookie);
            }
        %>
        <nav class="navbar navbar-inverse navbar-fixed-top" id = "bar">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Mailing Service</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <form class="navbar-form navbar-right" action="Login" method="post">
                        <div class="form-group">
                            <input type="email" placeholder="Email" class="form-control" name = "Email"> 
                        </div>
                        <div class="form-group">
                            <input type="password" placeholder="Password" class="form-control" name = "Password">
                        </div>
                        <button type="submit" class="btn btn-success">Sign in</button>
                    </form>
                </div><!--/.navbar-collapse -->
            </div>
        </nav>
        
        
        <div id ="full">
            <div class="container" id = "center">


                <%  
                    if (request.getAttribute("wrong") != null) {
                        String a = (String) request.getAttribute("wrong");
                        if (a.length() > 0) {
                            out.print("<div class =\"alert alert-danger\">" + a + "</div>");
                        }
                    }
                        
                %>

                <h1>Mailing Service</h1>
                <p>Sign up to join our E-mailing service.</p>
                <p><a class="btn btn-primary btn-lg" href="signUpForm.jsp" role="button">Sign Up</a></p>
            </div>
        </div>

        <script type="text/javascript">
            var x = $(window).height() - $("#bar").height();
            //alert(x);
            $("#full").height(x);
            $('#full').css('margin-top', $("#bar").height());
            $('#center').css('padding-top', $("#full").height() / 2 - $("#center").height());
            $(window).resize(function () {

                var x = $(window).height() - $("#bar").height();
                //alert(x);
                $("#full").height(x);
                $('#full').css('margin-top', $("#bar").height());
                $('#center').css('padding-top', $("#full").height() / 2 - $("#center").height());
            });
        </script>
        
      

