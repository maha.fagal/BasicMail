<%@page import="Models.Mail"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Mailing Service</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <style type="text/css">
            #full{

                background-color: #f1f1f1;

            }
            #center{
                color: white;
            }
            .show{
                background-color: white;
                height: 100%;
                padding-left: 3%;
            }

            .padd{
                padding-top: 1.5em;
                border-bottom: .1em solid grey;
                width: 100%;
            }

            .fontl{
                font-size: 1.5em;
            }

        </style>

    </head>

    <body >

        <nav class="navbar navbar-inverse navbar-fixed-top" id = "bar">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Mailing Service</a>
                </div>

                <div class="collapse navbar-collapse" id="navbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="inbox.jsp">Inbox</a></li>
                        <li><a href="new.html">New</a></li>
                        <li class=""><a href="archive.html">Archived</a></li>
                        <li><a href="search.jsp">search</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="index.jsp"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
                    </ul>
                </div><!--/.navbar-collapse -->
            </div>
        </nav>

        <div id ="full">
<!--            <div class="container" id = "mail_Container">-->
                <div class="row">
                    <div class="col-sm-1 "></div>
                    <div class="col-sm-10 show">
                        <table border="1">
                            <tr>

                                <td colspan="2">Subject</td>
                                <td colspan="2">Sender</td>
                                <td colspan="2">Receiver</td>
                                <td colspan="2">Status</td
                                <td colspan="2">Date</td>


                            </tr>
                            <%
                                String baseUrl = "showMail?mailID=";
                                ArrayList<Mail> mails = (ArrayList<Mail>) request.getAttribute("mails");
                                if (mails != null) {
                                    for (int i = 0; i < mails.size(); i++) {
                                        String url = baseUrl + mails.get(i).getId();
                            %>
                            <tr>
                                <td class="padd" colspan = "2"><a class = "fontl" href="<%=url%>"><%=mails.get(i).getMail(mails.get(i).getId()).getSubject()%></a></td>
                                <td class="padd" colspan=2"><a class = "fontl" href="<%=url%>"><%=mails.get(i).getMail(mails.get(i).getId()).getSender()%></a></td>
                                <td class="padd" colspan="2"><a class = "fontl" href="<%=url%>"><%=mails.get(i).getMail(mails.get(i).getId()).getReciever()%></a></td>
                                <td class="padd" colspan="2"><a class = "fontl" href="<%=url%>"><%=mails.get(i).getMail(mails.get(i).getId()).getStatus()%></a></td>
                                <td class="padd" colspan="2"><a class = "fontl" href="<%=url%>"><%=mails.get(i).getMail(mails.get(i).getId()).getDate()%></a></td>

                                <%
                                        }
                                    }

                                %>
                        </table>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            var x = $(window).height() - $("#bar").height();
            //alert(x);
            $("#full").height(x);
            $('#full').css('margin-top', $("#bar").height());
            $("#mail_Container").height(x);
            $(".show").height(x);

            $(window).resize(function () {
                var x = $(window).height() - $("#bar").height();
                //alert(x);
                $("#full").height(x);
                $('#full').css('margin-top', $("#bar").height());
                $("#mail_Container").height(x);
                $(".show").height(x);
            });

        </script>


