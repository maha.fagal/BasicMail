        
function sendSignUpData() {
            
            var firstName = document.getElementById("firstName").value;
            var lastName = document.getElementById("lastName").value;
            var mail = document.getElementById("mail").value;
            var pass1 = document.getElementById("pass1").value;
            var pass2 = document.getElementById("pass2").value;
            var query = "firstName=" + firstName + "&lastName=" + lastName + "&mail=" + mail +
                    "&pass1=" + pass1 + "&pass2=" + pass2;
            
            var xmlhttp = new XMLHttpRequest();

            xmlhttp.open("POST", "SignUp", true);
            xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            xmlhttp.send(query);
          
            xmlhttp.onreadystatechange = function ()
            {
                if (xmlhttp.readyState === 4 && xmlhttp.status === 200)
                {
                    if(xmlhttp.responseText !=="Success"){
                        document.getElementById("error_area").innerHTML = xmlhttp.responseText;
      
                        
                    }
                    else{
                        document.getElementById("error_area").innerHTML = "Registration Successful, You will be Redirected to the Login page";
                        document.getElementById("error_area").className = "alert alert-success";
                        window.setTimeout(window.location.replace("index.jsp"), 50);
                    }
                    
                    document.getElementById("error_area").style.display = 'block';
            }
            };

        }
            
